# 我要從什麼地方開始
FROM denoland/deno:alpine-1.26.0

# 我希望在這個 Image 裡面建立一個資料夾 (取名為 app)
WORKDIR /app

# 把左邊所有程式檔，整個複製到 app 資料夾 (. 指的是 here)
COPY . /app

# 這個專案跑起來的時候，在內部會開啟 8000 port
EXPOSE 8000

# 執行一些指令
RUN deno cache main.ts

# 把這個 server 跑起來
CMD [ "run", "--allow-all", "main.ts" ]
